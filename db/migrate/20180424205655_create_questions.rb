class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.text :question
      t.string :question_type
      t.belongs_to :subject, foreign_key: true

      t.timestamps
    end
  end
end
